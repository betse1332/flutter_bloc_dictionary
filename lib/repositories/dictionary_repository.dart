import 'package:flutter_bloc_dictionary/models/models.dart';
import 'package:flutter_bloc_dictionary/repositories/repository.dart';

class DictionaryRepository {
  DBHelper _dbHelper;
  DictionaryRepository() {
    _dbHelper = DBHelper.instance;
    print("_dbHelper is null ${_dbHelper == null}");
  }

  Future<List<List<HeadWord>>> searchForWord(String headWord, int offset) async {
    List<Map> words = await _dbHelper.searchForData(headWord, offset);
    // print(words);
    List<String> wordRecord = [];
    List<List<HeadWord>> listOfHeadWords = [];
    for (int i = 0; i < words.length; i++) {
      if (wordRecord.contains(words[i]["word"])) {
        continue;
      }
      wordRecord.add(words[i]["word"]);
      List<HeadWord> headWords = [];
      for (var word in words) {
        if (words[i]["word"].toString().compareTo(word['word'].toString()) ==
            0) {
          headWords.add(HeadWord.fromMap(word));
        }
      }
      listOfHeadWords.add(headWords);
    }
    // print(listOfHeadWords.toString());

    return listOfHeadWords == null ? [] : listOfHeadWords;
  }

  bool isLastData() {
    return _dbHelper.isLastData();
  }
}
