class HeadWord {
  String word;
  String wordType;
  String wordDefinition;
  static final String tableName="entries";

  HeadWord({this.word, this.wordType, this.wordDefinition});

  HeadWord.fromMap(Map<String, dynamic> map) {
    word = map["word"];
    wordType = map["wordtype"];
    wordDefinition = map["definition"];
  }

  @override
  String toString() {
   
    return "word :$word------wordType:$wordType-----wordDefinition:$wordDefinition";
  }
}
