import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc_dictionary/models/models.dart';

class SearchState extends Equatable {
  @override
  List<Object> get props => [];
}

class WordSearchInitial extends SearchState {}

class WordSearchInProgress extends SearchState {}

class WordSearchSuccess extends SearchState {
  final List<List<HeadWord>> wordList;
  final String headword;
  final int nextOffset;
  final bool isAllFetched;

  WordSearchSuccess({
    @required this.wordList,
    @required this.headword,
    @required this.nextOffset,
    @required this.isAllFetched
  })  : assert(wordList != null),
        assert(headword != null),
        assert(isAllFetched != null),
        assert(nextOffset != null);

  @override
  List<Object> get props => [this.wordList];
}

class WordSearchFailure extends SearchState {}
