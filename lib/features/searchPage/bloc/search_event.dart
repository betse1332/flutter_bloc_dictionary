import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class SearchEvent extends Equatable {
  const SearchEvent();
  @override
  List<Object> get props => [];
}

class WordSearchEvent extends SearchEvent {
  final String headWord;
  final int offset;

  const WordSearchEvent({@required this.headWord, @required this.offset})
      : assert(headWord != null),
        assert(offset != null);
  @override
  List<Object> get props => [headWord];
}

class WordSearchCancelledEvent extends SearchEvent {}
