import 'dart:io';
import 'package:flutter/services.dart';
import 'package:flutter_bloc_dictionary/models/models.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DBHelper {
  final String _databaseName = "Dictionary.db";
  Database _database;
  final int _searchLimit = 40;
  bool _isAllFetched = false;

  DBHelper._();
  static final DBHelper instance = DBHelper._();

  Future<Database> get _getdatabase async {
    try {
      if (_database != null) return _database;
      _database = await _initDatabase();

      return _database;
    } catch (e) {
      print("error in _getdatabase $e");
    }
  }

  _initDatabase() async {
    try {
      Directory directory = await getApplicationDocumentsDirectory();
      final path = join(directory.path, _databaseName);
      ByteData data = await rootBundle.load(join('assets', "mysqlite3.db"));

      List<int> bytes =
          data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      await File(path).writeAsBytes(bytes, flush: true);

      return await openDatabase(path);
    } catch (e) {
      print("error in _initDatabase $e");
    }
  }

  Future<List<Map>> searchForData(String query, int offset) async {
    try {
      Database database = await _getdatabase;
      print("log offset comming to database+++++++ $offset");
     
      final List<Map> data = await database.rawQuery(
          "SELECT * FROM ${HeadWord.tableName} WHERE word like ? LIMIT $_searchLimit OFFSET $offset",
          ["${query.trim()}%"]);
      // print("this is the query string $query");
      print("this length of the word list in dbhelper ${data.toString()}");
      if (data.length < _searchLimit) {
        _isAllFetched = true;
      }else{
        _isAllFetched = false;
      }
      return data == null ? [] : data;
    } catch (e) {
      print("error in searchForData $e");
    }
    return [];
  }

  bool isLastData() {
    return _isAllFetched;
  }

  // Future<List<Map>> fetch20Words(String query) async {
  //   List<Map> data = await searchForData(query);
  //   List<Map> subData = [];
  //   for (int i = 0; i < 20; i++) {
  //     subData.add(data[i]);
  //   }
  //   return subData;
  // }
}
