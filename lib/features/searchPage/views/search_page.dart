import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_bloc_dictionary/features/searchPage/search.dart';
import 'package:flutter_bloc_dictionary/models/models.dart';
import 'package:flutter_bloc_dictionary/repositories/repository.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
// import 'package:flutter_bloc_dictionary/features/search/search.dart';

import '../../../app_theme.dart';

class HomePage extends StatelessWidget {
  List<List<HeadWord>>_headWordList = [];
  @override
  Widget build(BuildContext context) {
    var containerWidth = MediaQuery.of(context).size.width;
    var containerHeight = MediaQuery.of(context).size.height / 4;
    var _keyboardVisibility = MediaQuery.of(context).viewInsets.bottom > 0;
    print("keyboardVisibility from home page scaffold $_keyboardVisibility");
    return Scaffold(
        body: SafeArea(
            child: BlocConsumer<SearchBloc, SearchState>(
      listener: (context, searchState) {
        if (searchState is WordSearchSuccess &&
            ((searchState).nextOffset > 20)) {
          _headWordList.addAll(searchState.wordList);
        } else if (searchState is WordSearchSuccess &&
            ((searchState).nextOffset == 20)) {
          _headWordList.clear();
          _headWordList.addAll(searchState.wordList);
        }
      },
      builder: (context, state) {
        return Stack(
          alignment: Alignment.topCenter,
          overflow: Overflow.visible,
          children: <Widget>[
            Container(
              height: containerHeight,
              width: containerWidth,
              color: AppTheme.background_dark_grey,
            ),
            Positioned(
              top: containerHeight * .83,
              width: containerWidth * .95,
              child: SearchWidget(),
            ),
            Align(
                alignment: Alignment.topCenter,
                child: Padding(
                    padding: EdgeInsets.fromLTRB(
                        AppSize.paddingM, 170, AppSize.paddingM, 0),
                    child: SearchLoader(
                      keyboardVisibility: _keyboardVisibility,
                      headWordList: _headWordList,
                    )))
          ],
        );
      },
    )));
  }
}
