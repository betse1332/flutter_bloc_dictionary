import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_dictionary/app_theme.dart';
import 'package:flutter_bloc_dictionary/features/searchPage/search.dart';

class SearchWidget extends StatefulWidget {
  @override
  _SearchWidgetState createState() => _SearchWidgetState();
}

class _SearchWidgetState extends State<SearchWidget> {
  final _visibilityNotifier = ValueNotifier<bool>(false);
  final _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _visibilityNotifier.dispose();
    _controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchBloc, SearchState>(builder: (context, state) {
      return Padding(
        padding: EdgeInsets.symmetric(horizontal: AppSize.paddingS),
        child: Material(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(5))),
          elevation: 10,
          shadowColor: Colors.grey[100],
          child: ClipRRect(
            borderRadius: BorderRadius.circular(5),
            child: TextField(
                cursorColor: Colors.grey,
                controller: _controller,
                textAlignVertical: TextAlignVertical.center,
                style: TextStyle(
                  decoration: TextDecoration.none,
                  fontSize: 20,
                ),
                onChanged: (value) {
                  print("search headWord $value");
                  value.length == 0
                      ? _visibilityNotifier.value = false
                      : _visibilityNotifier.value = true;
                  context
                      .read<SearchBloc>()
                      .add(WordSearchEvent(headWord: value, offset: 0));
                },
                decoration: InputDecoration(
                  prefixIcon: Icon(Icons.search),
                  suffixIcon: ValueListenableBuilder(
                      valueListenable: _visibilityNotifier,
                      builder: (_, value, __) => Visibility(
                            visible: value,
                            child: IconButton(
                              icon: Icon(Icons.cancel),
                              onPressed: () => {
                                context
                                    .read<SearchBloc>()
                                    .add(WordSearchCancelledEvent()),
                                _controller.value = TextEditingValue.empty,
                                _visibilityNotifier.value = false,
                              },
                            ),
                          )),
                  border: InputBorder.none,
                  hintText: "Search",
                  filled: true,
                  fillColor: Colors.white,
                )),
          ),
        ),
      );
    });
  }
}
// shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circu
