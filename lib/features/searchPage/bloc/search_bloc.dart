import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_dictionary/features/searchPage/bloc/search_bloc_export.dart';
import 'package:flutter_bloc_dictionary/models/models.dart';
import 'package:flutter_bloc_dictionary/repositories/repository.dart';
import 'package:rxdart/rxdart.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  final DictionaryRepository repository;
  final int offsetRaise = 20;

  SearchBloc({@required this.repository})
      : assert(repository != null),
        super(WordSearchInitial());

  @override
  Stream<SearchState> mapEventToState(SearchEvent event) async* {
    if (event is WordSearchEvent) {
      yield* _mapWordSearchEventToState(event);
    } else if (event is WordSearchCancelledEvent) {
      yield* _mapWordSearchCancelledEventToState(event);
    }
  }

  @override
  Stream<Transition<SearchEvent, SearchState>> transformEvents(
      Stream<SearchEvent> events,
      TransitionFunction<SearchEvent, SearchState> transitionFunction) {
    final nonDebounceStream =
        events.where((event) => event is! WordSearchEvent);
    final debounceStream = events
        .where((event) => event is WordSearchEvent)
        .debounceTime(Duration(milliseconds: 300));
    return super.transformEvents(
        MergeStream([nonDebounceStream, debounceStream]), transitionFunction);
  }

  Stream<SearchState> _mapWordSearchEventToState(WordSearchEvent event) async* {
    // print("search state offset: ${event.offset}");
    int nextOffset = event.offset + offsetRaise;
    print(
        "next offset from bloc: $nextOffset this is event offset ${event.offset}");
    if (event.headWord.isEmpty) {
      yield WordSearchInitial();
    } else {
      try {
        final wordList =
            await repository.searchForWord(event.headWord, event.offset);

        // print(wordList.toString());
        if (wordList.isEmpty) {
          wordList.add([HeadWord(word: "No match found")]);
        }

        if (state is WordSearchSuccess) {
          if ((state as WordSearchSuccess).headword == event.headWord) {
            final stateData = (state as WordSearchSuccess).wordList;
            stateData.addAll(wordList);
            // print("state data length ****** ${stateData[39]}");
            yield WordSearchSuccess(
                wordList: wordList,
                headword: event.headWord,
                isAllFetched: repository.isLastData(),
                nextOffset: nextOffset);
          } else {
            yield WordSearchSuccess(
                wordList: wordList,
                headword: event.headWord,
                isAllFetched: repository.isLastData(),
                nextOffset: nextOffset);
          }
        } else {
          yield WordSearchSuccess(
              wordList: wordList,
              isAllFetched: repository.isLastData(),
              headword: event.headWord,
              nextOffset: nextOffset);
        }
      } catch (e) {
        print("error in _mapWordSearchEventToState $e");
        yield WordSearchFailure();
      }
    }
  }

  Stream<SearchState> _mapWordSearchCancelledEventToState(
      WordSearchCancelledEvent event) async* {
    try {
      yield WordSearchInitial();
    } catch (e) {
      yield WordSearchFailure();
    }
  }
}
