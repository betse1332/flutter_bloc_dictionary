import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_dictionary/app_theme.dart';
import 'package:flutter_bloc_dictionary/features/searchPage/search.dart';
import 'package:flutter_bloc_dictionary/models/head_word.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class SearchLoader extends StatelessWidget {
  List<List<HeadWord>> headWordList;
  final _scrollController = ScrollController();
  final bool keyboardVisibility;
  SearchLoader({@required this.keyboardVisibility, @required this.headWordList})
      : assert(keyboardVisibility != null),
        assert(headWordList != null);
  // void _scrollListener() {
  //   var searchState = BlocProvider.of<SearchBloc>(context).state;

  //   if (_scrollController.position.pixels ==
  //           _scrollController.position.maxScrollExtent < 10 &&
  //       searchState is WordSearchSuccess) {
  //     print("this is the next offset ********${searchState.nextOffset}");
  //     BlocProvider.of<SearchBloc>(context).add(WordSearchEvent(
  //         headWord: searchState.headword, offset: searchState.nextOffset));
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    print("this is keyboardVisibility $keyboardVisibility");
    print(BlocProvider.of<SearchBloc>(context).state);
    print("length of state ${headWordList.length}");

    return BlocConsumer<SearchBloc, SearchState>(
        listener: (context, searchState) => {},
        builder: (context, state) {
          return state is WordSearchSuccess
              ? Visibility(
                  visible: keyboardVisibility,
                  child: Material(
                      elevation: 10,
                      child: Container(
                        height: 55.0 * (headWordList.length),
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: ListView.separated(
                            separatorBuilder:
                                (BuildContext context, int index) {
                              return Divider();
                            },
                            itemCount: headWordList.length,
                            // cacheExtent: 30,
                            controller: _scrollController
                              ..addListener(() {
                                if (_scrollController.offset ==
                                        _scrollController
                                            .position.maxScrollExtent &&
                                    !state.isAllFetched) {
                                  context.read<SearchBloc>().add(
                                      WordSearchEvent(
                                          headWord: state.headword,
                                          offset: state.nextOffset));
                                }
                              }),
                            // shrinkWrap: true,
                            itemBuilder: (BuildContext context, int index) {
                              print(
                                  "state length from builder ${headWordList.length}");
                              // print("data index: $index");
                              // int currentIndex = index - state.offset;
                              // print("currentIndex $currentIndex");

                              return Container(
                                // height: 30,
                                padding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 10),
                                child: Text(
                                  "${headWordList[index][0].word}",
                                  style: TextStyle(fontSize: 18),
                                ),
                              );
                            }),
                      )),
                )
              : Text("");
        });
  }
}
