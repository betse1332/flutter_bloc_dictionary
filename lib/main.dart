import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_dictionary/app_theme.dart';
import 'package:flutter_bloc_dictionary/features/search/search.dart';

import 'package:flutter_bloc_dictionary/repositories/repository.dart';
import 'package:flutter_bloc_dictionary/simple_bloc_observer.dart';

void main() {
  Bloc.observer = SimpleBlocObserver();

  final DictionaryRepository repository = DictionaryRepository();
  runApp(MyApp(
    repository: repository,
  ));
}

class MyApp extends StatelessWidget {
  final DictionaryRepository repository;
  MyApp({Key key, @required this.repository})
      : assert(repository != null),
        super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Flutter dictionary",
      theme: ThemeData(primaryColor: AppTheme.background_dark_grey),
      home: BlocProvider<SearchBloc>(
        create: (context) => SearchBloc(repository: repository),
        child: HomePage(),
      ),
    );
  }
}
