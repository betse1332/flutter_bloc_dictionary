import 'package:flutter/material.dart';

class AppTheme {
  static const Color background_dark_grey = Color(0xFF424242);
}

class AppSize {
  static const double paddingS = 8.0;
  static const double paddingM = 16.0;
  static const double paddingL = 32.0;
}
